export const acceptColor = (color) => ({
	type: "ACCEPT_COLOR",
	payload: color
});

export const getRemoteColorsSuccess = (data) => ({
	type: "GET_REMOTE_COLORS_SUCCESS",
	payload: data
});

export const getRemoteColorsFailure = (error) => ({
	type: "GET_REMOTE_COLORS_FAILURE",
	payload: error.message
});
