import React from "react";
import ReactDOM from "react-dom";
import store from "./store";
import { Provider } from "react-redux";

import ColorPicker  from "./components/ColorPicker";
import "./css/index.css";

ReactDOM.render(
	<Provider store={store}>
		<ColorPicker/>
	</Provider>, 
	document.getElementById("root")
);

