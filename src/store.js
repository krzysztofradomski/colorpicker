import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import colorpickerReducer from "./reducers/colorpickerReducer";

const middleware =  composeWithDevTools(applyMiddleware(thunk));

export default createStore(colorpickerReducer, middleware);

